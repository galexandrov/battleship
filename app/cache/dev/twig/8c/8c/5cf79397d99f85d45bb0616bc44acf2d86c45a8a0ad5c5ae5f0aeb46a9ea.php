<?php

/* GeorgiAlexandrovBattleshipBundle::layout.html.twig */
class __TwigTemplate_8c8c5cf79397d99f85d45bb0616bc44acf2d86c45a8a0ad5c5ae5f0aeb46a9ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
\t\t<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/GeorgiAlexandrovBattleship/css/style.css"), "html", null, true);
        echo "\" />
\t\t<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/GeorgiAlexandrovBattleship/js/jquery-1.11.1.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/GeorgiAlexandrovBattleship/js/script.js"), "html", null, true);
        echo "\"></script>
        <title>Georgi Alexandrov's Battleship</title>
    </head>
    <body>
    \t";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashBag"), "get", array(0 => "bombStatus"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 12
            echo "            ";
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "<br/>
    \t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "\t\t";
        $this->displayBlock('content', $context, $blocks);
        // line 15
        echo "    </body>
</html>";
    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "GeorgiAlexandrovBattleshipBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 14,  57 => 15,  54 => 14,  45 => 12,  41 => 11,  34 => 7,  30 => 6,  26 => 5,  20 => 1,);
    }
}
