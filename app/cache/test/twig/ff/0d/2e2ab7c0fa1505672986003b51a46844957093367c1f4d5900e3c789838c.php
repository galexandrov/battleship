<?php

/* GeorgiAlexandrovBattleshipBundle:Battleship:index.html.twig */
class __TwigTemplate_ff0d2e2ab7c0fa1505672986003b51a46844957093367c1f4d5900e3c789838c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GeorgiAlexandrovBattleshipBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GeorgiAlexandrovBattleshipBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "\t<p>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["board"]) ? $context["board"] : $this->getContext($context, "board")), "show", array(0 => (isset($context["showShips"]) ? $context["showShips"] : $this->getContext($context, "showShips"))), "method"), "html", null, true);
        echo "</p>
\t";
        // line 5
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashBag"), "get", array(0 => "gameover"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 6
            echo "        ";
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "<br/>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "    ";
        if (((isset($context["gameOver"]) ? $context["gameOver"] : $this->getContext($context, "gameOver")) == false)) {
            // line 9
            echo "\t\t";
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
            echo "
\t";
        } else {
            // line 11
            echo "\t\t<a href=\"/\">Play again?</a>
\t";
        }
    }

    public function getTemplateName()
    {
        return "GeorgiAlexandrovBattleshipBundle:Battleship:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 11,  52 => 9,  49 => 8,  40 => 6,  36 => 5,  31 => 4,  28 => 3,);
    }
}
