<?php
namespace GeorgiAlexandrov\BattleshipBundle\Classes\Builder;

use GeorgiAlexandrov\BattleshipBundle\Models\Ship;

class Shipyard
{
	const DESTROYER = 4;
	const BATTLESHIP = 5;
	
	public static function makeShip($shipType, $maxSize) 
	{
		$x = rand(0, $maxSize);
		$y = rand(0, $maxSize);
		$length = $shipType;
		$direction = rand(0,1);
		return new Ship($x, $y, $length, $direction);
	}
}