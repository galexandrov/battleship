<?php
namespace GeorgiAlexandrov\BattleshipBundle\Classes\Adapters;

use GeorgiAlexandrov\BattleshipBundle\Interfaces\Adapters\IBoardOutputAdapter;
use GeorgiAlexandrov\BattleshipBundle\Models\Map;

class WebOutputAdapter implements IBoardOutputAdapter
{
	private $_map;
	
	public function __construct(Map $map)
	{
		$this->_map = $map;
	}
	
	public function show($withShips)
	{
		echo '&nbsp;';
		for ($i=0; $i<$this->_map->getSize();$i++) {
			echo $i;
		}
		echo "<br/>";
		foreach ($this->_map->show($withShips) as $row => $cols) {
			echo chr(65+$row);
			foreach ($cols as $index =>$col) {
				echo $col;
			}
			echo "<br/>";
		}
		echo "<br/>";
	}
}