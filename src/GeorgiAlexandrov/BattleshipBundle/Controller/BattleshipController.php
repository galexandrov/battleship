<?php
namespace GeorgiAlexandrov\BattleshipBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\FormError;
use Psr\Log\InvalidArgumentException;
use GeorgiAlexandrov\BattleshipBundle\Models\BattleshipGame;
use GeorgiAlexandrov\BattleshipBundle\Models\Map;
use GeorgiAlexandrov\BattleshipBundle\Classes\Adapters\WebOutputAdapter;

class BattleshipController extends Controller {
	
	public function indexAction(Request $request) {
		$session = $this->getRequest()->getSession();
		$showShips = false;
		$gameOver = false;
		
		$form = $this->createFormBuilder()
			->add('coordinates', 'text', array('label' => $this->get('translator')->trans('Enter coordinates (row, col), e.g. A5 = ')))
			->add('save', 'submit', array('label'=>$this->get('translator')->trans("Submit")))
			->getForm();
		
		$form->handleRequest($request);
		
		$game = $session->get('game');
		
		if (empty($game) || empty($form->getData()['coordinates'])) {
			$game = new BattleshipGame();
			$session->set('game', $game);
		}
		
		if ($request->getMethod() == 'POST') {
			$input = $form->getData()['coordinates'];
			
			switch ($input) {
				case "show":
					$showShips = true;
				break;
				default:
					if (!empty($input)) {
						try {
							$status =$game->shoot(strtoupper($input));
							
							if ($status == Map::HIT) $session->getFlashBag()->add('bombStatus', 'Hit');
							if ($status == Map::SINK) $session->getFlashBag()->add('bombStatus', 'Sink');
							if ($status == Map::MISS) $session->getFlashBag()->add('bombStatus', 'Miss');
							if ($status == BattleshipGame::GAME_OVER) {
								$session->getFlashBag()->add('gameover', "You finish in ".$game->getNumberOfShoots().' moves');
								$session->set('game', new BattleshipGame());
								$gameOver = true;
							}
						} catch (InvalidArgumentException $e) {
							$form->get('coordinates')->addError(new FormError($e->getMessage()));
						}
					}
			}
		}
				
		return $this->render('GeorgiAlexandrovBattleshipBundle:Battleship:index.html.twig', 
			array (
				'board' => new WebOutputAdapter($game->getMap()),
				'showShips' => $showShips,
				'gameOver' => $gameOver,
				'form' => $form->createView() 
			)
		);
	}
}
