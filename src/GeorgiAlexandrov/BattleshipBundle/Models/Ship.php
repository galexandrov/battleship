<?php
namespace GeorgiAlexandrov\BattleshipBundle\Models;

class Ship
{
	const EAST = 0;
	const SOUTH = 1;
	
	private static $ships=1;
	private $_id;
	private $_length;
	private $_coordinate_x;
	private $_coordinate_y;
	private $_direction;
	private $_damage;
	
	public function __construct($x, $y, $length, $direction)
	{
		$this->_id = self::$ships++;
		$this->_coordinate_x = $x;
		$this->_coordinate_y = $y;
		$this->_length = $length;
		$this->_direction= $direction;
		$this->_damage = array_fill(0, $this->_length, false);
	}
	
	public function getId()
	{
		return $this->_id;
	}
	
	public function getX()
	{
		return $this->_coordinate_x;
	}
	
	public function getY()
	{
		return $this->_coordinate_y;
	}
	public function getDirection()
	{
		return $this->_direction;
	}
	
	public function getLength()
	{
		return $this->_length;
	}
	
	/**
	 * Seas are dangerous, thorpedos and bombs are coming from unexpected directions.
	 * 
	 * @param integer $part Unlucky sector of the ship
	 */
	public function hit($sector)
	{
		$this->_damage[$sector] = true;
		$this->_length--;
	}
	
	/**
	 * Returning the state of the ship. Sunk when all sectors of the ship are
	 * damaged by the {@link #hit($sector) hit} method
	 * 
	 * @return boolean
	 */
	public function isSunk()
	{
		return $this->_length == 0;
	}
	
	/**
	 * Is the sector of the ship hit by the {@link #hit($sector) hit} method
	 * 
	 * @param boolean
	 */
	public function isSectorDamaged($sector)
	{
		return $this->_damage[$sector];
	}

}