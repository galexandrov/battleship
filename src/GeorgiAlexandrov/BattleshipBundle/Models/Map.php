<?php
namespace GeorgiAlexandrov\BattleshipBundle\Models;

use GeorgiAlexandrov\BattleshipBundle\Models\Ship;
use GeorgiAlexandrov\BattleshipBundle\Classes\Builder\Shipyard;

class Map
{
	const HIT = "HIT";
	const SINK = "SINK";
	const MISS = "MISS";
	private $_size = 10;
	private $_map;
	private $_ships = array();
	
	public function __construct($size)
	{
		$this->_size = $size;
		
		$this->_map = array_fill(0, $this->_size, array_fill(0, $this->_size, ''));
	}
	
	public function getSize()
	{
		return $this->_size;
	}
	
	public function getShips()
	{
		return $this->_ships;
	}
	
	public function setValueAtMapSector($x, $y, $value)
	{
		$this->_map[$x][$y] = $value;
	}
	
	public function getValueAtMapSector($x, $y)
	{
		return $this->_map[$x][$y];
	}
	
	/**
	 * Validate if ship can be at location
	 *
	 * @param \Ship $newShip the new ship
	 * @return boolean
	 */
	public function validateLocation(Ship $newShip)
	{
		if ($newShip->getX()+$newShip->getLength() > $this->_size) return false;
		if ($newShip->getY()+$newShip->getLength() > $this->_size) return false;
	
		if (empty($this->_ships)) return true;
	
		if ($newShip->getDirection() == Ship::EAST) {
			for ($i=$newShip->getX();$i<=$newShip->getX()+$newShip->getLength();$i++) {
				if (!empty($this->_map[$i][$newShip->getY()])) return false;
			}
		} else {
			for ($i=$newShip->getY();$i<=$newShip->getY()+$newShip->getLength();$i++) {
				if (!empty($this->_map[$newShip->getX()][$i])) return false;
			}
		}
	
		return true;
	}
	
	/**
	 * Shoot in the map, see if any ship is hit or sunk.
	 *
	 * @param string $coordinate Coordinate in format [A-Z][0-9]
	 */
	public function shoot($x, $y)
	{
		$sector = $this->getValueAtMapSector($x, $y);
		
		if ($sector != false && $sector != Map::MISS) {
			$ship = $this->_ships[$sector];
			if ($ship->getDirection() == Ship::EAST) {
				$ship->hit($x-$ship->getX());
			} else {
				$ship->hit($y-$ship->getY());
			}
			if ($ship->isSunk()) {
				$warEnd = false;
				foreach ($this->_ships as $ship) {
					if (!$ship->isSunk()) $warEnd = true;
				}
				if (!$warEnd) {
					return BattleshipGame::GAME_OVER;
				}
				return Map::SINK;
			} else {
				return Map::HIT;
			}
		} else {
			$this->setValueAtMapSector($x, $y, Map::MISS);
			return Map::MISS;
		}
	
	}
	
	/**
	 * Add a new ship on the map
	 * 
	 * @param \Ship $ship the new ship
	 * @return boolean
	 */
	public function addShipOnMap(Ship $ship)
	{
		if (!$this->validateLocation($ship)) return false;
		
		$this->_ships[$ship->getId()] = $ship;
		
		$start = $ship->getDirection() == Ship::EAST ? $ship->getX() : $ship->getY();
		
		for ($index=$start;$index<$start+$ship->getLength();$index++) {
			if ($ship->getDirection() == Ship::EAST) {
				$this->_map[$index][$ship->getY()] = $ship->getId();
			} else {
				$this->_map[$ship->getX()][$index] = $ship->getId();
			}
		}
		return true;
	}
	
	/**
	 * Generating an array representing the state of the map
	 * 
	 * @param boolean $showShips should ships be visible
	 * @return Two dimentional array representing the map
	 */
	public function show($showShips=false)
	{
		$mapToShow = array();
		foreach ($this->_map as $key=>$row) {
			$mapToShow[$key] = array();
			foreach ($row as $ind=>$column) {
				if ($row==0) $mapToShow[$key][] = $ind;
				$valueAtSector = $this->getValueAtMapSector($ind, $key);
				if (empty($valueAtSector)) {
					$mapToShow[$key][] = $showShips ? ' ' : '.';
				} else {
					if ($valueAtSector == Map::MISS) {
						$mapToShow[$key][] = $showShips ? ' ' : '-';
						continue;
					}
					$ship = $this->_ships[$valueAtSector];
						
					$sector = $ship->getDirection() == Ship::EAST ? $ind-$ship->getX() : $key-$ship->getY();
						
					if ($ship->isSectorDamaged($sector)) {
						$mapToShow[$key][] = $showShips ? ' ' : 'X';
					} else {
						$mapToShow[$key][] = $showShips ? 'X' : '.';
					}
				}
			}
		}
		return $mapToShow;
	}
	
}