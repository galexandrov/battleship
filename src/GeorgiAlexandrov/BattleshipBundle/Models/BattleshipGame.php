<?php
namespace GeorgiAlexandrov\BattleshipBundle\Models;

use GeorgiAlexandrov\BattleshipBundle\Models\Map;
use Psr\Log\InvalidArgumentException;
use GeorgiAlexandrov\BattleshipBundle\Classes\Builder\Shipyard;

class BattleshipGame
{
	const GAME_OVER = "GAME OVER";
	
	private $_size = 10;
	private $_shipsToCreate = array(Shipyard::BATTLESHIP, Shipyard::DESTROYER, Shipyard::DESTROYER);
	private $_numberOfShoots=0;
	private $_map= array();
	
	public function __construct($withShips = true)
	{
		$this->_map = new Map($this->_size);
		
		foreach ($this->_shipsToCreate as $shipToCreate) {
			$isShipOk = false;
			do {
				$ship = Shipyard::makeShip($shipToCreate, $this->_size);
				if ($this->_map->addShipOnMap($ship)) {
					$isShipOk = true;
				}
			} while (!$isShipOk);
		}
	}
	
	public function shoot($coordinate)
	{
		preg_match('/^(?P<y>\w+)(?P<x>\d+)$/', $coordinate, $coordinates);
		if (!isset($coordinates['x']) || !isset($coordinates['y'])) {
			throw new InvalidArgumentException("Invalid input", 100);
		}
		$x = $coordinates['x'];
		$y = ord($coordinates['y'])-65;
		if ($x > $this->_size || $y > $this->_size) {
			throw new InvalidArgumentException("Value outside the box", 110);
		}
		$this->_numberOfShoots++;
		
		return $this->_map->shoot($x, $y);
	}
	
	public function getNumberOfShoots()
	{
		return $this->_numberOfShoots;
	}
	
	public function setMap(Map $map)
	{
		$this->_map = $map;
	}
	
	public function getMap($showShips=false)
	{
		return $this->_map;
		
	}
}