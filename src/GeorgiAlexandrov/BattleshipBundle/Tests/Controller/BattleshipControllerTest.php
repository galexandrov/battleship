<?php

namespace GeorgiAlexandrov\BattleshipBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use GeorgiAlexandrov\BattleshipBundle\Models\Ship;
use GeorgiAlexandrov\BattleshipBundle\Models\Map;
use GeorgiAlexandrov\BattleshipBundle\Models\BattleshipGame;

class BattleshipControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        
        $crawler = $client->request('GET', '/');

        $this->assertTrue($crawler->filter('html:contains("Enter coordinates (row, col), e.g. A5 =")')->count() > 0);
        
        $x1 = 1;
        $y1 = 3;
        $length1 = 5;
        $direction1 = Ship::EAST;
        
       
        $x3 = 4;
        $y3 = 5;
        $length3 = 4;
        $direction3 = Ship::SOUTH;
        
        $map = new Map(10);
        
        $ship1 = new Ship($x1, $y1, $length1, $direction1);
        $map->addShipOnMap($ship1);
        $this->assertEquals(1, count($map->getShips()));
        
        $ship3 = new Ship($x3, $y3, $length3, $direction3);
        $map->addShipOnMap($ship3);
        
        $this->assertEquals(2, count($map->getShips()));
        
        $game = new BattleshipGame();
        $game->setMap($map);
        
        $container = $client->getContainer();
        $session = $container->get('session');
        
        
        $game = $session->get('game');
        foreach ($game->getMap()->show(true) as $key=>$row) {
        	foreach ($row as $col=>$value) {
        		if ($value == 'X') {
        			$crawler = $client->request('POST', '/', array('form' => array('coordinates'=> chr($key+65).$col)));
        			$hit = $crawler->filter('body:contains("Hit")')->count() > 0;
        			$sink = $crawler->filter('body:contains("Sink")')->count() > 0;
        			$gameOver = $crawler->filter('body:contains("You finish")')->count() > 0;
        			if ($gameOver) {
        				preg_match('/You finish in (?P<moves>\d+) moves/s', $crawler->filter('body')->text(), $moves);
        				$this->assertEquals(13, $moves['moves'], "Incorrect number of moves calculation");
        			}
        			$this->assertTrue($hit || $sink || $gameOver);
        		}
        	}
        }
    }
}
