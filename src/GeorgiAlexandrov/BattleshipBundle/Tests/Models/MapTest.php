<?php
namespace GeorgiAlexandrov\BattleshipBundle\Test;

use Psr\Log\InvalidArgumentException;
use GeorgiAlexandrov\BattleshipBundle\Models\BattleshipGame;
use GeorgiAlexandrov\BattleshipBundle\Models\Map;
use GeorgiAlexandrov\BattleshipBundle\Models\Ship;
use GeorgiAlexandrov\BattleshipBundle\Classes\Adapters\ConsoleOutputAdapter;

class MapTest extends \PHPUnit_Framework_TestCase
{
	public function testCreatingBoard()
	{
		$battleship = new BattleshipGame();
		
		$this->assertEquals(3, count($battleship->getMap()->getShips()));
	}
	
	public function testBoardValidationForCrowOverlap()
	{
		$x1 = 1;
		$y1 = 3;
		$length1 = 5;
		$direction1 = Ship::EAST;
		
		$x2 = 1;
		$y2 = 3;
		$length2 = 4;
		$direction2 = Ship::SOUTH;
		
		$x3 = 4;
		$y3 = 5;
		$length3 = 4;
		$direction3 = Ship::SOUTH;
		
		$map = new Map(10);
		
		$ship1 = new Ship($x1, $y1, $length1, $direction1);
		$map->addShipOnMap($ship1);
		$this->assertEquals(1, count($map->getShips()));
		
		$ship2 = new Ship($x2, $y2, $length2, $direction2);
		$map->addShipOnMap($ship2);
		$this->assertEquals(1, count($map->getShips()));
		
		$ship3 = new Ship($x3, $y3, $length3, $direction3);
		$map->addShipOnMap($ship3);
		
		$this->assertEquals(2, count($map->getShips()));

		$game = new BattleshipGame();
		$game->setMap($map);
		
		try{
			$shoot = $game->shoot('bla');
			$this->fail("Invalid input");
		}catch(InvalidArgumentException $e){
			$this->assertEquals(100,$e->getCode());
			$this->assertEquals("Invalid input",$e->getMessage());
		}
		
		try{
			$shoot = $game->shoot('Z10');
			$this->fail("Value outside the box");
		}catch(InvalidArgumentException $e){
			$this->assertEquals(110,$e->getCode());
			$this->assertEquals("Value outside the box",$e->getMessage());
		}

		$shoot = $game->shoot('A1');
		$this->assertEquals($shoot, Map::MISS);
		
		$shoot = $game->shoot('D1');
		$this->assertEquals($shoot, Map::HIT);

		$shoot = $game->shoot('D2');
		$this->assertEquals($shoot, Map::HIT);
				
		$shoot = $game->shoot('D3');
		$this->assertEquals($shoot, Map::HIT);
				
		$shoot = $game->shoot('D4');
		$this->assertEquals($shoot, Map::HIT);
				
		$shoot = $game->shoot('D5');
		$this->assertEquals($shoot, Map::SINK);
				
		$shoot = $game->shoot('F4');
		$this->assertEquals($shoot, Map::HIT);
				
		$shoot = $game->shoot('G4');
		$this->assertEquals($shoot, Map::HIT);
				
		$shoot = $game->shoot('H4');
		$this->assertEquals($shoot, Map::HIT);
				
		$shoot = $game->shoot('I4');
		$this->assertEquals($shoot, BattleshipGame::GAME_OVER);
		
		$this->assertEquals(10, $game->getNumberOfShoots());
	}
}