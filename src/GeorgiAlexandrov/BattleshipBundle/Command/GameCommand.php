<?php
namespace GeorgiAlexandrov\BattleshipBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\InvalidArgumentException;
use GeorgiAlexandrov\BattleshipBundle\Models\BattleshipGame;
use GeorgiAlexandrov\BattleshipBundle\Models\Map;
use GeorgiAlexandrov\BattleshipBundle\Classes\Adapters\ConsoleOutputAdapter;

class GameCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('game')
			->setDescription('Play some battleship game!');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$game = new BattleshipGame();
		
		$dialog = $this->getHelperSet()->get('dialog');
		
		$gameConsoleOutput = new ConsoleOutputAdapter($game->getMap());
		$output->write($gameConsoleOutput->show(false));
		do {
			$input = $dialog->askAndValidate($output, 'Enter coordinates (row, col), e.g. A5 = ', function($answer) {
				if (!preg_match('/^(\bshow|\bexit|([\w]+[\d]+))$/', $answer)) { 
					throw new \RuntimeException('Bad command! Type exit to quit!');
				}
				return $answer;
			});
			
			switch ($input) {
				case "show":
					$gameConsoleOutput = new ConsoleOutputAdapter($game->getMap());
					$output->write($gameConsoleOutput->show(true));
				break;
				case "exit":
					$output->writeln("Good bye!");
				break;
				default:
					try {
						$status = $game->shoot(strtoupper($input));
						$gameConsoleOutput = new ConsoleOutputAdapter($game->getMap());
						
						$output->write($gameConsoleOutput->show(false));
						
						if ($status == Map::HIT) $output->writeln("Hit");
						if ($status == Map::SINK) $output->writeln("Sink");
						if ($status == Map::MISS) $output->writeln("Miss");
						if ($status == BattleshipGame::GAME_OVER) {
							$output->writeln("You finish in ".$game->getNumberOfShoots().' moves');
							$input = 'exit';
						}
					} catch (InvalidArgumentException $e) {
						$output->writeln($e->getMessage());
					}
			}
		} while ($input !== 'exit');
	}
}