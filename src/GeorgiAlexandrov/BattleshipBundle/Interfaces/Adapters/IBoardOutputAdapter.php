<?php
namespace GeorgiAlexandrov\BattleshipBundle\Interfaces\Adapters;

use GeorgiAlexandrov\BattleshipBundle\Models\Board;

interface IBoardOutputAdapter
{
	public function show($withShips);
}